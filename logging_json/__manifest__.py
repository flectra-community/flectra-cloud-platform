# Copyright 2016-2019 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

{
    "name": "JSON Logging",
    "version": "1.0.1.0.0",
    "author": "Camptocamp,Odoo Community Association (OCA), Jamotion GmbH",
    "license": "AGPL-3",
    "category": "Extra Tools",
    "depends": ["base"],
    "external_dependencies": {"python": ["python-json-logger"]},
    "website": "https://gitlab.com/flectra-community/flectra",
    "data": [],
    "installable": True,
}
