# Copyright 2012-2019 Camptocamp SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl)
{
    "name": "Base FileURL Field",
    "summary": "Implementation of FileURL type fields",
    "version": "1.0.1.0.0",
    "category": "Technical Settings",
    "author": "Camptocamp, Odoo Community Association (OCA), Jamotion GmbH",
    "website": "https://gitlab.com/flectra-community/flectra",
    "license": "AGPL-3",
    "depends": ["base_attachment_object_storage"],
    "auto_install": False,
    "installable": True,
}
