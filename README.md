# None





Available addons
----------------

addon | version | summary
--- | --- | ---
[monitoring_statsd](monitoring_statsd/) | 1.0.1.0.0| Monitoring: Statsd Metrics
[monitoring_log_requests](monitoring_log_requests/) | 1.0.1.0.0| Monitoring: Requests Logging
[attachment_swift](attachment_swift/) | 1.0.1.0.0| Store assets and attachments on a Swift compatible object store
[test_base_fileurl_field](test_base_fileurl_field/) | 1.0.1.0.0| A module to verify fileurl field.
[logging_json](logging_json/) | 1.0.1.0.0| JSON Logging
[session_redis](session_redis/) | 1.0.1.0.0| Store web sessions in Redis
[cloud_platform_azure](cloud_platform_azure/) | 1.0.1.0.0| Addons required for the Camptocamp Cloud Platform on Azure
[cloud_platform_exoscale](cloud_platform_exoscale/) | 1.0.2.0.0| Addons required for the Camptocamp Cloud Platform on Exoscale
[base_attachment_object_storage](base_attachment_object_storage/) | 1.0.1.1.0| Base module for the implementation of external object store.
[monitoring_status](monitoring_status/) | 1.0.1.0.0| Monitoring: Status
[attachment_azure](attachment_azure/) | 1.0.1.0.0| Store assets and attachments on a Azure compatible object storage
[attachment_s3](attachment_s3/) | 1.0.1.0.0| Store assets and attachments on a S3 compatible object storage
[base_fileurl_field](base_fileurl_field/) | 1.0.1.0.0| Implementation of FileURL type fields
[cloud_platform](cloud_platform/) | 1.0.2.0.0| Addons required for the Camptocamp Cloud Platform
[cloud_platform_ovh](cloud_platform_ovh/) | 1.0.2.0.1| Addons required for the Camptocamp Cloud Platform on OVH
[monitoring_prometheus](monitoring_prometheus/) | 1.0.1.0.0| Monitoring: Prometheus Metrics


